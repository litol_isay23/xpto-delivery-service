<?php

namespace App\Domain\User\Repository;

use App\Domain\User\Data\UserAuthData;
use PDO;

/**
 * Repository.
 */
class UserRepository {
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection) {
        $this->connection = $connection;
    }

    /**
     * Get user and permissions.
     *
     * @param UserAuthData $userDetails username, password
     *
     * @return array user_id, username, permission_id, permissions
     */
    public function getUserAndPermissions(UserAuthData $userDetails): array {
        // Query with named placeholders
        $where = [
            "username"      => $userDetails->username,
            "password"      => md5($userDetails->password),
            "is_active"     => 1,
            "is_enabled"    => 1
        ];

        // Select structure
        $query = "SELECT 
                    u.id AS user_id, 
                    u.username, 
                    p.id AS permission_id, 
                    p.action 
                FROM users u
                LEFT JOIN user_permissions up 
                    ON up.user_id = u.id
                LEFT JOIN permissions p 
                    ON p.id = up.permission_id
                WHERE
                    u.username=:username
                AND
                    u.password=:password
                AND 
                    u.is_active=:is_active
                AND 
                    up.is_enabled=:is_enabled;";

        // Run query
        $sqlStatement = $this->connection->prepare($query);
        $sqlStatement->execute($where);
        $result = $sqlStatement->fetchAll();

        // Return result
        return $result;
    }

}