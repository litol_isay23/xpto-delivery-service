<?php

namespace App\Domain\User\Data;

final class UserCreateData {
    /** @var string */
    public $username;

    /** @var string */
    public $password;

    /** @var array */
    public $permissions;

}