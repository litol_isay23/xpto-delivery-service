<?php

namespace App\Domain\User\Data;

final class UserWithPermissionData {

    /** @var int */
    public $userId;

    /** @var string */
    public $username;

    /** @var array */
    public $permissions;

    /** @var string */
    public $errorMsg;

}