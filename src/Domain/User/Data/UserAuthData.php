<?php

namespace App\Domain\User\Data;

final class UserAuthData {

    /** @var string */
    public $username;

    /** @var string */
    public $password;

}