<?php

namespace App\Domain\User\Service;

use App\Domain\User\Data\UserAuthData;
use App\Domain\User\Data\UserWithPermissionData;
use App\Domain\User\Repository\UserRepository;
use InvalidArgumentException;

/**
 * Service.
 */
final class UserService {
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * The constructor.
     *
     * @param UserRepository $repository The repository
     */
    public function __construct(UserRepository $repository) {
        $this->repository = $repository;
    }

    /**
     * Get user and permissions.
     *
     * @param string $dataAuth Basic Auth data
     *
     * @throws InvalidArgumentException
     *
     * @return UserWithPermissionData 
     */
    public function getUserByUsernameAndPassword(string $dataAuth): UserWithPermissionData {
        // Decode Basic Auth
        $decodedAuth = base64_decode(str_replace("Basic ", "", $dataAuth));

        // Check if existing user
        $authDetails = explode(":", $decodedAuth);

        // Map to UserAuthData
        $user = new UserAuthData();
        $user->username = $authDetails[0];
        $user->password = $authDetails[1];

        // Validation
        if (empty($user->username) || empty($user->password)) {
            throw new InvalidArgumentException('Username and password cannot be empty');
        }

        // Get user
        $result = $this->repository->getUserAndPermissions($user);

        // Map to UserWithPermissionData
        $userDetails = new UserWithPermissionData();
        $userDetails->permissions = [];
        $userDetails->userId = 0;
        $userDetails->username = $user->username;

        // Validate $result is an array
        if (!is_array($result)) {
            $userDetails->errorMsg = 'An error has been encountered';

            return $userDetails;
        }

        // Validate $result length is greater than 0
        if (sizeof($result) == 0) {
            $userDetails->errorMsg = 'User does not exist';

            return $userDetails;
        }
        
        // Assign details to UserWithPermissionData
        $userDetails->userId = (int)$result[0]['user_id'];
        $userDetails->username = $result[0]['username'];

        // Loop over $result to get permissions
        foreach ($result as $value) {
            array_push($userDetails->permissions, $value['action']);
        }

        return $userDetails;
    }
}