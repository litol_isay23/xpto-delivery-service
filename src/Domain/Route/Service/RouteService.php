<?php

namespace App\Domain\Route\Service;

use App\Domain\Route\Data\RoutesOriginDestData;
use App\Domain\Route\Data\RouteUpdatePtRouteData;
use App\Domain\Route\Data\RoutePerPtData;
use App\Domain\Route\Data\RoutesReturnData;
use App\Domain\Route\Data\RouteInsertReturnData;
use App\Domain\Route\Repository\RouteRepository;
use InvalidArgumentException;

/**
 * Service.
 */
final class RouteService {
    /**
     * @var RouteRepository
     */
    private $repository;

    /**
     * The constructor.
     *
     * @param RouteRepository $repository The repository
     */

    public function __construct(RouteRepository $repository) {
        $this->repository = $repository;
    }

    /**
     * Get routes given origin and destination.
     *
     * @param RoutesOriginDestData $routePt origin and destination data
     *
     * @throws InvalidArgumentException
     *
     * @return RoutesReturnData found array of @param RoutePerPtData , errorMsg 
     */
    public function getAllRoutesByOriginAndDest(RoutesOriginDestData $routePt): RoutesReturnData {
        // Validation
        if (empty($routePt->ptOrigin) || empty($routePt->ptDest)) {
            throw new InvalidArgumentException('Origin and destination cannot be empty');
        }

        // Init value 
        $routePt->routeId = 0;
        $finResult = new RoutesReturnData();

        // First query
        $initResult = $this->repository->getRoutes($routePt);

        // Validate returned $initResult
        if (sizeof($initResult) <= 1) {
            $finResult->errorMsg = "Origin or destination is invalid";
            $finResult->found = [];

            return $finResult;
        }

        // Multiple possible routes
        $timeBetPts = [];
        $costBetPts = [];
        $idPerRoute = [];
        $ptPair = [];
        $idAlreadyFound = [];

        $firstOriginCheck = [];
        $firstDestCheck = [];

        // For rows with request = origin: Get dest
        $orgRequestDest = [];
        // For rows with request = destination: Get origin
        $destRequestOrg = [];

        // Possible start and sure end (destination)
        $originPos = [];
        $destPos = [];

        // First formatting
        foreach ($initResult as $res) {
            $firstOriginCheck[] = $res['origin']; 
            $firstDestCheck[] = $res['dest'];

            $pairing = $res['origin'].'|'.$res['dest'];

            $timeBetPts[$pairing] = (int)$res['time'];
            $costBetPts[$pairing] = (float)$res['cost'];
            $idPerRoute[$pairing] = (int)$res['id'];
            $ptPair[] = strtoupper($pairing);
            $idAlreadyFound[] = (int)$res['id'];

            // Origin and Destination sort out
            if ($res['request'] == 'origin') {
                $orgRequestDest[] = strtoupper($res['dest']);
                $originPos[] = strtoupper($pairing);
            }

            if ($res['request'] == 'destination') {
                $destRequestOrg[] = strtoupper($res['origin']);
                $destPos[] = strtoupper($pairing);
            }

        }

        // Checks for shortest route eg. A->C->B
        $shortestRoute = array_intersect($firstOriginCheck, $firstDestCheck);

        if (sizeof($shortestRoute) > 0) {
            foreach ($shortestRoute as $short) {
                $shortRt = [$short];
                // Combines altogether
                array_unshift($shortRt, $routePt->ptOrigin);
                $shortRt[] = $routePt->ptDest;
                $formatArr = [];
                $keyToBeDeleted = [];

                for ($i = 0; $i < 3; $i++) {
                    if ($i < 2) {
                        $keyToBeDeleted[] = $shortRt[$i].'|'.$shortRt[$i+1];

                        $perRouteDetails = new RoutePerPtData();

                        $perRouteDetails->id    = (int)$idPerRoute[$shortRt[$i].'|'.$shortRt[$i+1]];
                        $perRouteDetails->from  = $shortRt[$i];
                        $perRouteDetails->to    = $shortRt[$i+1];
                        $perRouteDetails->time  = (int)$timeBetPts[$shortRt[$i].'|'.$shortRt[$i+1]];
                        $perRouteDetails->cost  = (float)$costBetPts[$shortRt[$i].'|'.$shortRt[$i+1]];

                        $formatArr[] = $perRouteDetails;
                    }
                }

                $finResult->found[implode('|', $shortRt)] = $formatArr;

                // Remove from ptPair originPos and destPos
                foreach ($keyToBeDeleted as $value) {
                    if(($key = array_search(strtoupper($value), $ptPair)) !== false) {
                        unset($ptPair[$key]);
                    }   
                    // if(($key = array_search(strtoupper($value), $originPos)) !== false) {
                    //     unset($originPos[$key]);
                    // }   
                    // if(($key = array_search(strtoupper($value), $destPos)) !== false) {
                    //     unset($destPos[$key]);
                    // }   
                }
                $ptPair = array_values($ptPair);
                // $originPos = array_values($originPos);
                // $destPos = array_values($destPos);

                // Remove from $orgRequestDest and $destRequestOrg
                if(($key = array_search(strtoupper($short), $orgRequestDest)) !== false) {
                    unset($orgRequestDest[$key]);
                }   
                if(($key = array_search(strtoupper($short), $destRequestOrg)) !== false) {
                    unset($destRequestOrg[$key]);
                }   
                $orgRequestDest = array_values($orgRequestDest);
                $destRequestOrg = array_values($destRequestOrg);
            }
        }
        $extOrgPos = [];
        // $extDestPos = [];

        // Continous loop until no db result combination is found
        while(sizeof($orgRequestDest) > 0 || sizeof($destRequestOrg) > 0) {
            // Second Query
            $queryParams = new RoutesOriginDestData();

            $queryParams->routeId = $idAlreadyFound;
            $queryParams->ptOrigin = $orgRequestDest;
            $queryParams->ptDest = $destRequestOrg;

            $result = $this->repository->getRoutes($queryParams);

            $oldOrgRequestDest = $orgRequestDest;
            $oldDestRequestOrg = $destRequestOrg;

            $destRequestOrg = [];
            $orgRequestDest = [];

            // Check result
            foreach ($result as $ind => $res) {
                
                if ($res['request'] == 'origin') {
                    // Search through $originPos to find $res['origin] in each end string
                    foreach ($originPos as $key => $value) { 
                        if (strpos(strtoupper($value), strtoupper('|'.$res['origin'])) !== false) {
                            // Found: insert extended to $extOrgPos
                            $extOrgPos[] = $value.'|'.$res['dest'];
                        }
                    }
                }

                if ($res['request'] == 'destination') {
                    // Search through $destPos to find $res['dest] in each start string
                    foreach ($destPos as $key => $value) { 
                        if (strpos(strtoupper($value), strtoupper($res['dest'].'|')) !== false) {
                            // Found: Change value 
                            $destPos[] = $res['origin'].'|'.$value;
                        }
                    }
                }

                $pairing = $res['origin'].'|'.$res['dest'];

                $idPerRoute[$pairing] = (int)$res['id'];
                $timeBetPts[$pairing] = (int)$res['time'];
                $costBetPts[$pairing] = (float)$res['cost'];
                $ptPair[] = strtoupper($pairing);
                $idAlreadyFound[] = (int)$res['id'];

                // New assingment to $orgRequestDest
                if (!in_array($res['dest'], $oldOrgRequestDest)) {
                    $orgRequestDest[] = $res['dest'];
                }

                // New assignment to $destRequestOrg
                if (!in_array($res['dest'], $oldDestRequestOrg)) {
                    $destRequestOrg[] = $res['origin'];
                }
            }
        }

        // Merging $originPos and $destPos
        $routes = [];

        $originPos = $extOrgPos;
        // $destPos = array_merge($destPos, $extDestPos);

        foreach ($originPos as $val) {
            $fPts = explode('|', $val);

            foreach ($destPos as $key => $dVal) { 
                // When end of $val in start of $dVal
                if (strpos(strtoupper($dVal), strtoupper($fPts[sizeof($fPts)-1].'|')) !== false) {
                    $destPosArr = explode('|', $dVal);
                    unset($destPosArr[0]);
                    $destPosArr = array_values($destPosArr);
                    $routes[] = implode('|', $fPts).'|'.implode('|', $destPosArr);
                } else {
                    // When there is/are missing connections
                    foreach ($ptPair as $pKey => $pair) {

                        // Search in ptPair
                        if (strpos(strtoupper($pair), strtoupper($fPts[sizeof($fPts)-1].'|')) !== false) {
                            $ptPairArr = explode('|', $pair);

                            $routes[] = implode('|', $fPts).'|'.$ptPairArr[1].'|'.$dVal;
                        }
                    }
                }
            }
        }

        // Formatting/mapping to $finResult->found
        foreach ($routes as $key => $route) {
            $routeArr = explode('|', $route);
            $formatArr = [];

            for ($i = 0; $i < sizeof($routeArr); $i++) {
                if ($i < sizeof($routeArr)-1) {

                    if (isset($idPerRoute[$routeArr[$i].'|'.$routeArr[$i+1]]) && isset($timeBetPts[$routeArr[$i].'|'.$routeArr[$i+1]]) && isset($costBetPts[$routeArr[$i].'|'.$routeArr[$i+1]])) {
                        $perRouteDetails = new RoutePerPtData();

                        $perRouteDetails->from  = $routeArr[$i];
                        $perRouteDetails->to    = $routeArr[$i+1];

                        $perRouteDetails->id  = (int)$idPerRoute[$routeArr[$i].'|'.$routeArr[$i+1]];
                        $perRouteDetails->time  = (int)$timeBetPts[$routeArr[$i].'|'.$routeArr[$i+1]];
                        $perRouteDetails->cost  = (float)$costBetPts[$routeArr[$i].'|'.$routeArr[$i+1]];

                        $formatArr[] = $perRouteDetails;
                    } else {
                        $formatArr = [];
                        break;
                    }
                }
            }

            if (sizeof($formatArr) > 0) {
                $finResult->found[$route] = $formatArr;
            }
            
        }

        // Return $finResult
        return $finResult;
    }
    /**
     * Insert new routes or new points
     *
     * @param array array of @param RoutePerPtData
     *
     * @throws InvalidArgumentException
     *
     * @return RouteInsertReturnData 
     */
    public function createRouteOrPoint(array $routes):RouteInsertReturnData {
        $insertPt = [];
        $insertRt = [];

        // Retrieve available points to compare with to be inserted
        $arrPoints = [];

        $pointAvailable = $this->repository->getAvailablePoints();

        foreach ($pointAvailable as $val) {
            $arrPoints[$val['id']] = $val['point_letter'];
        }

        // Make sure to insert only valid/non empty element
        foreach ($routes as $route) {
            // Points
            if (!empty($route['to']) && !in_array($route['to'], $arrPoints)) {
                $insertPt[] = $route['to'];
            }
            if (!empty($route['from']) && !in_array($route['from'], $arrPoints)) {
                $insertPt[] = $route['from'];
            }
            // Route
            if ((!empty($route['time']) || !empty($route['cost'])) && !empty($route['to']) && !empty($route['from'])) {
                if (in_array($route['from'], $arrPoints)) {
                    if(($key = array_search(strtoupper($route['from']), $arrPoints)) !== false) {
                        $route['from'] = (int)$key;
                    }  
                } 

                if (in_array($route['to'], $arrPoints)) {
                    if(($key = array_search(strtoupper($route['to']), $arrPoints)) !== false) {
                        $route['to'] = (int)$key;
                    }  
                }

                if (!isset($route['time'])) {
                    $route['time'] = 0;
                }

                if (!isset($route['cost'])) {
                    $route['cost'] = 0;
                }

                $insertRt[] = $route;
            }
        }

        // Map to data
        $returnRes = new RouteInsertReturnData();

        $returnRes->pointCount = !empty($insertPt) ? $this->repository->insertPt($insertPt) : 0;
        $returnRes->routeCount = $this->repository->insertRoute($insertRt);

        return $returnRes;
    }
    /**
     * Update point name/letter
     *
     * @param RouteUpdatePtRouteData
     *
     * @throws InvalidArgumentException
     *
     * @return  
     */
    public function updatePoint(RouteUpdatePtRouteData $ptData):array {
        $result = $this->repository->updatePointName($ptData);
        
        return [
            "updatePointCount" => $result
        ];
    }
    /**
     * Update route cost or time
     *
     * @param RouteUpdatePtRouteData
     *
     * @throws InvalidArgumentException
     *
     * @return  
     */
    public function updateRoute(RouteUpdatePtRouteData $routeData):array {
        $result = $this->repository->updateRouteCostOrTime($routeData);
        
        return [
            "updateRouteCount" => $result
        ];
    }
    /**
     * Delete point
     *
     * @param array int
     *
     * @throws InvalidArgumentException
     *
     * @return  
     */
    public function deletePoint($id):array {
        $result = $this->repository->deletePoint($id);
        
        return [
            "deletePointCount" => $result
        ];
    }
    /**
     * Delete route
     *
     * @param array int
     *
     * @throws InvalidArgumentException
     *
     * @return  
     */
    public function deleteRoute($id):array {
        $result = $this->repository->deleteRoute($id);
        
        return [
            "deleteRouteCount" => $result
        ];
    }
    

}