<?php

namespace App\Domain\Route\Repository;

use App\Domain\Route\Data\RouteUpdatePtRouteData;
use App\Domain\Route\Data\RoutesOriginDestData;
use PDO;

/**
 * Route Repository
 */
class RouteRepository {
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection) {
        $this->connection = $connection;
    }

    /**
     * Get all possible route given origin and destination
     *
     * @param RoutesOriginDestData $deliveryRoute origin, destination
     *
     * @return array from_pt, to_pt, time, cost
     */
    public function getRoutes(RoutesOriginDestData $deliveryRoute): array {
        $whereNotId = '';
        $whereInOrigin = '';
        $whereInDest = '';

        $query = '';
        $enableNotInOrginOrDest = false;
        $operatorDest = '';
        $operatorOrigin = '';

        // Check param data type for $deliveryRoute->routeId
        if (is_array($deliveryRoute->routeId) && sizeof($deliveryRoute->routeId) > 0) {
            $whereNotId = ' AND ptc.id NOT IN (' . implode(',', $deliveryRoute->routeId) . ')';
        } else if (is_integer($deliveryRoute->routeId) && $deliveryRoute->routeId > 0) {
            $whereNotId = ' AND ptc.id != '.$deliveryRoute->ptOrigin;
        } else {
            $enableNotInOrginOrDest = true;
        }

        // Check param data type for $deliveryRoute->ptOrigin
        if (is_array($deliveryRoute->ptOrigin) && sizeof($deliveryRoute->ptOrigin) > 0) {
            $whereInOrigin = 'IN ("' . implode('","', $deliveryRoute->ptOrigin) . '")';
            $operatorOrigin = 'NOT ';
        } else if (is_string($deliveryRoute->ptOrigin) && strlen($deliveryRoute->ptOrigin) > 0) {
            $whereInOrigin = '= "'.$deliveryRoute->ptOrigin.'"';
            $operatorOrigin = '!';
        }

        // Check param data type for $deliveryRoute->ptDest
        if (is_array($deliveryRoute->ptDest) && sizeof($deliveryRoute->ptDest) > 0) {
            $whereInDest = 'IN ("' . implode('","', $deliveryRoute->ptDest) . '")';
            $operatorDest = 'NOT ';
        } else if (is_string($deliveryRoute->ptDest) && strlen($deliveryRoute->ptDest) > 0) {
            $whereInDest = '= "'.$deliveryRoute->ptDest.'"';
            $operatorDest = '!';
        }

        if ($whereInOrigin != '' || $whereInDest != '') {
            $query .= 'SELECT a.* FROM (';
        }

        // Check if proceed with origin query
        if ($whereInOrigin != '') {
            $whereNotInDest = $enableNotInOrginOrDest && $operatorDest != '' ? ' AND t.point_letter '.$operatorDest.$whereInDest : '';

            $query .= 'SELECT
                         ptc.id, 
                         ptc.time, 
                         ptc.cost, 
                         fr.point_letter AS origin, 
                         t.point_letter AS dest, 
                         "origin" AS request
                     FROM points_time_cost ptc
                     LEFT JOIN 
                         route_points fr
                     ON 
                         fr.id = ptc.route_point_origin
                     LEFT JOIN 
                         route_points t
                     ON 
                         t.id = ptc.route_point_dest
                     WHERE 
                         ptc.is_active = 1
                     AND
                         fr.point_letter '.$whereInOrigin.$whereNotInDest.$whereNotId;
        }

        // Check if proceed with destination query
        if ($whereInDest != '') {
            if (strlen($query) > 17) {
                $query .= ' UNION ';
            }

            $whereNotInOrigin = $enableNotInOrginOrDest && $operatorOrigin != '' ? ' AND fr.point_letter '.$operatorOrigin.$whereInOrigin : '';

            $query .=  'SELECT
                            ptc.id, 
                            ptc.time, 
                            ptc.cost, 
                            fr.point_letter AS origin, 
                            t.point_letter AS dest, 
                            "destination" AS request
                        FROM points_time_cost ptc
                        LEFT JOIN 
                            route_points fr
                        ON 
                            fr.id = ptc.route_point_origin
                        LEFT JOIN 
                            route_points t
                        ON 
                            t.id = ptc.route_point_dest
                        WHERE 
                            ptc.is_active = 1
                        AND
                            t.point_letter '.$whereInDest.$whereNotInOrigin.$whereNotId;
        }

        if ($whereInOrigin != '' || $whereInDest != '') {
            $query .= ') a GROUP BY a.id';

            // Run query
            $sqlStatement = $this->connection->prepare(htmlspecialchars_decode($query));
            $sqlStatement->execute();
            $result = $sqlStatement->fetchAll();

            // Return result
            return $result;
        }
    }
    /**
     * Get all points available
     *
     * @return array 
     */
    public function getAvailablePoints(): array {
        $query = 'SELECT 
                    *
                 FROM 
                    route_points';
        
        // Run query
        $sqlStatement = $this->connection->prepare(htmlspecialchars_decode($query));
        $sqlStatement->execute();
        $result = $sqlStatement->fetchAll();

        // Return result
        return $result;
    }
    /**
     * Insert to route_points
     *
     * @return int 
     */
    public function insertPt(array $points) {

        $query = 'INSERT INTO
                      route_points
                      (point_letter)
                  VALUES ';

        $qPart = array_fill(0, count($points), "(?)");
        $query .=  implode(",",$qPart);

        $sqlStatement = $this->connection->prepare(htmlspecialchars_decode($query)); 

        $i = 1;
        foreach($points as $item) { 
            $sqlStatement->bindValue($i++, $item);
        }
        
        // Run query
        $sqlStatement->execute();

        return $sqlStatement->rowCount();
    }
    /**
     * Insert to route_points
     *
     * @return int 
     */
    public function insertRoute(array $routes) {

        $insertVal = '';

        foreach($routes as $key => $route) {

            if (!is_int($route['from'])) {
                $routes[$key]['from'] = $this->getPointId($route['from']);
            }
            if (!is_int($route['to'])) {
                $routes[$key]['to'] = $this->getPointId($route['to']);
            }

            $routeId = $this->getIfRouteExists(array('from' => $routes[$key]['from'], 'to' => $routes[$key]['to']));

            if ($routeId > 0) {
                unset($routes[$key]);
            }
        }

        $routes = array_values($routes);

        if (sizeof($routes) > 0) {
            $query = 'INSERT INTO
                      points_time_cost
                      (
                          route_point_origin,
                          route_point_dest,
                          time,
                          cost
                      )
                  VALUES ';

            $qPart = array_fill(0, count($routes), "(?, ?, ?, ?)");
            $query .=  implode(",",$qPart);

            $sqlStatement = $this->connection->prepare(htmlspecialchars_decode($query)); 

            $i = 1;
            foreach($routes as $item) { 
                $sqlStatement->bindValue($i++, $item['from']);
                $sqlStatement->bindValue($i++, $item['to']);
                $sqlStatement->bindValue($i++, $item['time']);
                $sqlStatement->bindValue($i++, $item['cost']);
            }

            // Run query
            $sqlStatement->execute();

            // Return rows
            return $sqlStatement->rowCount();
        }

        return 0;
        
    }
    /**
     * Get id given point
     *
     * @return int 
     */
    private function getPointId($strPt) {
        $query = 'SELECT 
                    id
                 FROM 
                    route_points
                 WHERE
                    point_letter = "'.$strPt.'"';
        
        // Run query
        $sqlStatement = $this->connection->prepare(htmlspecialchars_decode($query));
        $sqlStatement->execute();
        return $sqlStatement->fetch()['id'];
    }
    /**
     * Get if route already exists
     *
     * @return int 
     */
    private function getIfRouteExists(array $point): int {
        $where = [
            "route_point_origin"    => $point['from'],
            "route_point_dest"      => $point['to'],
            "is_active"             => 1
        ];

        $query = 'SELECT 
                    id
                 FROM 
                    points_time_cost
                 WHERE
                    route_point_origin=:route_point_origin
                AND
                    route_point_dest=:route_point_dest
                AND
                    is_active=:is_active';
        
        // Run query
        $sqlStatement = $this->connection->prepare(htmlspecialchars_decode($query));
        $sqlStatement->execute($where);
        
        return (int)$sqlStatement->fetch()['id'];
    }
     /**
     * Update point name
     *
     * @return int 
     */
    public function updatePointName(RouteUpdatePtRouteData $point): int {
        $row = [
            'point_letter'  => $point->ptString,
            'id'            => $point->id
        ];

        $query = 'UPDATE
                    route_points
                 SET
                    point_letter=:point_letter
                 WHERE
                    id=:id';

        $sqlStatement = $this->connection->prepare(htmlspecialchars_decode($query));
        $sqlStatement->execute($row);

        return $sqlStatement->rowCount();

    }
     /**
     * Update route cost or time
     *
     * @return int 
     */
    public function updateRouteCostOrTime(RouteUpdatePtRouteData $route): int {
        $row = [];

        $query = 'UPDATE
                    points_time_cost
                 SET ';

        if ($route->time != null) {
            $row['time'] = $route->time;
            $query .= 'time=:time';
        }

        if ($route->cost != null) {
            if ($route->time != null) {
                $query .= ', ';
            }
            $row['cost'] = $route->cost;
            $query .= 'cost=:cost';
        }

        $row[id] = $route->id;
        $query .= ' WHERE id=:id';

        $sqlStatement = $this->connection->prepare(htmlspecialchars_decode($query));
        $sqlStatement->execute($row);

        return $sqlStatement->rowCount();

    }

    /**
     * Delete point
     *
     * @return int 
     */
    public function deletePoint($id): int {
        $count = 0;
        $updCount = 0;

        if (is_array($id)) {

            for($i=0; $i < count($id); $i++) {
                $query = 'DELETE 
                          FROM 
                            route_points 
                          WHERE 
                            id = :id';

                $sqlStatement = $this->connection->prepare(htmlspecialchars_decode($query));
                $sqlStatement->execute(array(':id' => $id[$i]));
                $count += $sqlStatement->rowCount();
            }
        } 

        if ($count > 0) {
            $query = 'UPDATE 
                        points_time_cost
                      SET
                        is_active = 0
                      WHERE 
                        id IN ('.implode(',', $id).')';

            $sqlStatement = $this->connection->prepare(htmlspecialchars_decode($query));
            $sqlStatement->execute();
            // $updCount = $sqlStatement->rowCount();
        }

        return $count;
    }
    /**
     * Delete route
     *
     * @return int 
     */
    public function deleteRoute($id): int {
        $query = 'UPDATE 
                        points_time_cost
                      SET
                        is_active = 0
                      WHERE 
                        id IN ('.implode(',', $id).')';

        $sqlStatement = $this->connection->prepare(htmlspecialchars_decode($query));
        $sqlStatement->execute();
        return $sqlStatement->rowCount();
    }
}