<?php

namespace App\Domain\Route\Data;

final class RoutesOriginDestData {

    /** @var array or
     *  @var int
     */
    public $routeId;    

    /** @var array or
     *  @var string
     */
    public $ptOrigin;    

    /** @var array or
     *  @var string
     */
    public $ptDest;
}