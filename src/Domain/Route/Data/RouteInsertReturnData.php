<?php

namespace App\Domain\Route\Data;

final class RouteInsertReturnData {

    /** 
     *  @var int
     */
    public $pointCount;    

    /** 
     *  @var int
     */
    public $routeCount;
}