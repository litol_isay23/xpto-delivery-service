<?php

namespace App\Domain\Route\Data;

final class RouteUpdatePtRouteData {

    /** 
     *  @var id
     */
    public $id;    

    /** 
     *  @var string
     */
    public $ptString;    

    /** 
     *  @var float
     */
    public $cost;

    /** 
     *  @var int
     */
    public $time;
}