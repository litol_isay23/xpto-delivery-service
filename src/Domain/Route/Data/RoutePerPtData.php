<?php

namespace App\Domain\Route\Data;

final class RoutePerPtData {

    /** 
     *  @var int
     */
    public $id;    

    /** 
     *  @var string
     */
    public $from;    

    /** 
     *  @var string
     */
    public $to;

    /** 
     *  @var int
     */
    public $time;

    /** 
     *  @var float
     */
    public $cost;
}