<?php

namespace App\Domain\Route\Data;

final class RoutesReturnData {

    /** 
     * @var RoutePerPtData
     */
    public $found;    

    /** 
     *  @var string
     */
    public $errorMsg;    
}