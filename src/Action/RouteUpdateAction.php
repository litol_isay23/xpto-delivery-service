<?php

namespace App\Action;

use App\Domain\User\Service\UserService;
use App\Domain\Route\Service\RouteService;
use App\Domain\Route\Data\RouteUpdatePtRouteData;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class RouteUpdateAction {
    private $userService;
    private $routeService;

    public function __construct(UserService $userService, RouteService $routeService) {
        $this->userService = $userService;
        $this->routeService = $routeService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        // Collect Headers
        $dataHeaders = (array)$request->getHeaders();

        // Collect input from the HTTP request
        $dataRequest = (array)$request->getParsedBody();

        // Validate $dataRequest and $dataHeaders
        if (!isset($dataHeaders['Authorization']) || !isset($dataRequest['update_request']) || !isset($dataRequest['id']) || empty($dataHeaders['Authorization'][0]) || empty($dataRequest['update_request']) || empty($dataRequest['id'])) {
            // Return error message
            $returnRes = [
                "error"     => true,
                "message"   => ['Basic Auth authentication is required', '[`id`] and [`update_request`] cannot be empty']
            ];

            $response->getBody()->write(json_encode($returnRes));

            return $response->withHeader('Content-Type', 'application/json')->withStatus(422);
        }

        // Call to UserService util
        $userDetails = $this->userService->getUserByUsernameAndPassword($dataHeaders['Authorization'][0]);

        // Check if $userDetails returned an error
        if ($userDetails->userId == 0 && $userDetails->errorMsg != null) {
            // Return error message
            $returnRes = [
                "error"     => true,
                "message"   => $userDetails->errorMsg
            ];

            $response->getBody()->write(json_encode($returnRes));

            return $response->withHeader('Content-Type', 'application/json')->withStatus(422);

        } 
        
        if (!in_array("UPDATE", $userDetails->permissions)) {
            // Return lack permission message
            $returnRes = [
                "error"     => true,
                "message"   => "User is not allowed to perform this request"
            ];

            $response->getBody()->write(json_encode($returnRes));

            return $response->withHeader('Content-Type', 'application/json')->withStatus(422);
        } 

        $updateRes = [];

        // If point name/letter is to be updated
        if ($dataRequest['update_request'] == "point" && isset($dataRequest['point']) && !empty($dataRequest['point'])) {
            $objRequest = new RouteUpdatePtRouteData();
            $objRequest->id = $dataRequest['id'];
            $objRequest->ptString = $dataRequest['point'];

            $updateRes = $this->routeService->updatePoint($objRequest);

        } else if ($dataRequest['update_request'] == "route" && (isset($dataRequest['time']) || isset($dataRequest['cost']))) {
            
            // If cost or time is to be updated
            $objRequest = new RouteUpdatePtRouteData();
            $objRequest->id = $dataRequest['id'];
            $objRequest->cost = isset($dataRequest['cost']) ? $dataRequest['cost'] : null;
            $objRequest->time = isset($dataRequest['time']) ? $dataRequest['time'] : null;

            if ($objRequest->cost != null || $objRequest->time != null) {
                $updateRes = $this->routeService->updateRoute($objRequest);
            } 
        } else {
            $updateRes = ['errorMsg' => 'Invalid update route request'];
        }

        $returnRes = [
            "success"                               => true,
            "user_id"                               => $userDetails->userId,
            "username"                              => $userDetails->username,
            "permissions"                           => $userDetails->permissions,
            "update_".$dataRequest['update_request']=> ['request' => $dataRequest, 'response'  => $updateRes]
        ];

        $response->getBody()->write(json_encode($returnRes));

        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}