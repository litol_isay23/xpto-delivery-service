<?php

namespace App\Action;

use App\Domain\User\Data\UserAuthData;
use App\Domain\Route\Data\RoutesOriginDestData;
use App\Domain\User\Service\UserService;
use App\Domain\Route\Service\RouteService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class RouteGetAction {
    private $userService;
    private $routeService;

    public function __construct(UserService $userService, RouteService $routeService) {
        $this->userService = $userService;
        $this->routeService = $routeService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        // Collect Headers
        $dataHeaders = (array)$request->getHeaders();

        // Collect input from the HTTP request
        $dataRequest = (array)$request->getParsedBody();

        // Init return var
        $returnRes = [];

        // Validate $dataRequest
        if (!isset($dataRequest['origin']) || !isset($dataRequest['destination']) || !isset($dataHeaders['Authorization']) || empty($dataRequest['origin']) || empty($dataRequest['destination']) || empty($dataHeaders['Authorization'][0]) || strtoupper($dataRequest['origin']) == strtoupper($dataRequest['destination'])) {
            // Return error message
            $returnRes = [
                "error"     => true,
                "message"   => ['Basic Auth authentication is required', '[`origin`] and [`destination`] are required fields', '[`origin`] and [`destination`] must not be the same']
            ];

            $response->getBody()->write(json_encode($returnRes));

            return $response->withHeader('Content-Type', 'application/json')->withStatus(422);
        } 

        // Map $dataRequest to RoutesOriginDestData
        $ptDetails = new RoutesOriginDestData();
        $ptDetails->ptOrigin = $dataRequest['origin'];
        $ptDetails->ptDest = $dataRequest['destination'];

        // Call to UserService util
        $userDetails = $this->userService->getUserByUsernameAndPassword($dataHeaders['Authorization'][0]);

        // Check if $userDetails returned an error
        if ($userDetails->userId == 0 && $userDetails->errorMsg != null) {
            // Return error message
            $returnRes = [
                "error"     => true,
                "message"   => $userDetails->errorMsg
            ];

            $response->getBody()->write(json_encode($returnRes));

            return $response->withHeader('Content-Type', 'application/json')->withStatus(422);

        } 
        
        if (!in_array("READ", $userDetails->permissions)) {
            // Return lack permission message
            $returnRes = [
                "error"     => true,
                "message"   => "User is not allowed to perform this request"
            ];

            $response->getBody()->write(json_encode($returnRes));

            return $response->withHeader('Content-Type', 'application/json')->withStatus(422);
        } 
        
        // Call to RouteService
        $routesArr = $this->routeService->getAllRoutesByOriginAndDest($ptDetails);

        $returnRes = [
            "success"       => true,
            "user_id"       => $userDetails->userId,
            "username"      => $userDetails->username,
            "permissions"   => $userDetails->permissions,
            "routes"        => ['request' => $dataRequest, 'response'  => $routesArr]
        ];

        $response->getBody()->write(json_encode($returnRes));

        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}