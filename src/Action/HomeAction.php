<?php

namespace App\Action;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class HomeAction {
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $result = ['success' => true, 'message' => 'This program is developed by Aisa D. Lamdag. Please give proper credits when using this program.'];

        $response->getBody()->write(json_encode($result));

        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}