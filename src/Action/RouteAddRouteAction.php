<?php

namespace App\Action;

use App\Domain\User\Service\UserService;
use App\Domain\Route\Service\RouteService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class RouteAddRouteAction {
    private $userService;
    private $routeService;

    public function __construct(UserService $userService, RouteService $routeService) {
        $this->userService = $userService;
        $this->routeService = $routeService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        // Collect Headers
        $dataHeaders = (array)$request->getHeaders();

        // Collect input from the HTTP request
        $dataRequest = (array)$request->getParsedBody();

        // Validate $dataRequest and $dataHeaders
        if (!isset($dataHeaders['Authorization']) || !isset($dataRequest['body']) || empty($dataHeaders['Authorization'][0]) || empty($dataRequest['body']) ) {
            // Return error message
            $returnRes = [
                "error"     => true,
                "message"   => ['Basic Auth authentication is required', '[`body`] cannot be empty']
            ];

            $response->getBody()->write(json_encode($returnRes));

            return $response->withHeader('Content-Type', 'application/json')->withStatus(422);
        }

        // Call to UserService util
        $userDetails = $this->userService->getUserByUsernameAndPassword($dataHeaders['Authorization'][0]);

        // Check if $userDetails returned an error
        if ($userDetails->userId == 0 && $userDetails->errorMsg != null) {
            // Return error message
            $returnRes = [
                "error"     => true,
                "message"   => $userDetails->errorMsg
            ];

            $response->getBody()->write(json_encode($returnRes));

            return $response->withHeader('Content-Type', 'application/json')->withStatus(422);

        } 
        
        if (!in_array("CREATE", $userDetails->permissions)) {
            // Return lack permission message
            $returnRes = [
                "error"     => true,
                "message"   => "User is not allowed to perform this request"
            ];

            $response->getBody()->write(json_encode($returnRes));

            return $response->withHeader('Content-Type', 'application/json')->withStatus(422);
        } 

        // Call to RouteService - create
        $createRes = $this->routeService->createRouteOrPoint($dataRequest['body']);

        $returnRes = [
            "success"           => true,
            "user_id"           => $userDetails->userId,
            "username"          => $userDetails->username,
            "permissions"       => $userDetails->permissions,
            "add_point_route"   => ['request' => $dataRequest, 'response'  => $createRes]
        ];

        $response->getBody()->write(json_encode($returnRes));

        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}