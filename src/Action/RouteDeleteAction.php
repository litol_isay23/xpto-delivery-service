<?php

namespace App\Action;

use App\Domain\User\Service\UserService;
use App\Domain\Route\Service\RouteService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class RouteDeleteAction {
    private $userService;
    private $routeService;

    public function __construct(UserService $userService, RouteService $routeService) {
        $this->userService = $userService;
        $this->routeService = $routeService;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        // Collect Headers
        $dataHeaders = (array)$request->getHeaders();

        // Collect input from the HTTP request
        $dataRequest = (array)$request->getParsedBody();

        // Validate $dataRequest and $dataHeaders
        if (!isset($dataHeaders['Authorization']) || !isset($dataRequest['id']) || empty($dataHeaders['Authorization'][0]) || !is_array($dataRequest['id']) || empty($dataRequest['id'])) {
            // Return error message
            $returnRes = [
                "error"     => true,
                "message"   => ['Basic Auth authentication is required', '[`id`] and [`delete_request`] cannot be empty', '[`id`] should be an array of int']
            ];

            $response->getBody()->write(json_encode($returnRes));

            return $response->withHeader('Content-Type', 'application/json')->withStatus(422);
        }

        // Call to UserService util
        $userDetails = $this->userService->getUserByUsernameAndPassword($dataHeaders['Authorization'][0]);

        // Check if $userDetails returned an error
        if ($userDetails->userId == 0 && $userDetails->errorMsg != null) {
            // Return error message
            $returnRes = [
                "error"     => true,
                "message"   => $userDetails->errorMsg
            ];

            $response->getBody()->write(json_encode($returnRes));

            return $response->withHeader('Content-Type', 'application/json')->withStatus(422);

        } 
        
        if (!in_array("DELETE", $userDetails->permissions)) {
            // Return lack permission message
            $returnRes = [
                "error"     => true,
                "message"   => "User is not allowed to perform this request"
            ];

            $response->getBody()->write(json_encode($returnRes));

            return $response->withHeader('Content-Type', 'application/json')->withStatus(422);
        } 

        // $deleteRes = [];
        
        // if ($dataRequest['delete_request'] == "point") {
        //     $deleteRes = $this->routeService->deletePoint($dataRequest['id']);

        // } else 

        // if ($dataRequest['delete_request'] == "route") {
            $deleteRes = $this->routeService->deleteRoute($dataRequest['id']);   
        // }

        $returnRes = [
            "success"                               => true,
            "user_id"                               => $userDetails->userId,
            "username"                              => $userDetails->username,
            "permissions"                           => $userDetails->permissions,
            "update_".$dataRequest['delete_request']=> ['request' => $dataRequest, 'response'  => $deleteRes]
        ];

        $response->getBody()->write(json_encode($returnRes));

        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}