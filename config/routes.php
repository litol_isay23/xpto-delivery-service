<?php

use Slim\App;

return function (App $app) {
    $app->get('/', \App\Action\HomeAction::class);
    $app->get('/route_points', \App\Action\RouteGetAction::class);
    $app->post('/route_points', \App\Action\RouteAddRouteAction::class);
    $app->put('/route_points', \App\Action\RouteUpdateAction::class);
    $app->delete('/route_points', \App\Action\RouteDeleteAction::class);
};
