# XPTO Delivery Service

This is a REST API which:
● Retrieve the routes within a defined origin and destination points;
● Manage points and routes (implement the CRUD);

## Initial Setup/Installation
1. Import the database `xpto_delivery.sql`
2. Open source code path: config/settings.php
3. Change the lines of database configuration setup after the comment: 

> // Change host, username, and password depending on your local's db setup

4.  On your terminal/bash go to the root folder of this project, then run 
> composer install
5. If you don't have a composer yet, open this [link](https://getcomposer.org/download/) guide you through your installation
6. Once everything has been set, run php serve or if you have XAMPP/LAMP/MAMP check using your browser or Postman app
> http://localhost/xpto-delivery-service/

## SlimPHP Framework

This program is built using the SlimPHP Framework. It is a micro framework that is easy to use and very light-weight. To learn more about SlimPHP visit their [site](http://www.slimframework.com/)

## To Start Testing 

You may use the included Postman request file `xpto_delivery.postman_collection.json` or access and download [this](https://www.getpostman.com/collections/579df3ec1677770aa0fa)
