-- Adminer 4.7.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `action` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `action` (`action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `permissions` (`id`, `action`) VALUES
(1,	'CREATE'),
(2,	'DELETE'),
(4,	'READ'),
(3,	'UPDATE');

DROP TABLE IF EXISTS `points_time_cost`;
CREATE TABLE `points_time_cost` (
  `id` int NOT NULL AUTO_INCREMENT,
  `route_point_origin` int NOT NULL,
  `route_point_dest` int NOT NULL,
  `time` int NOT NULL,
  `cost` double NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `route_point_from` (`route_point_origin`),
  KEY `route_point_to` (`route_point_dest`),
  CONSTRAINT `points_time_cost_ibfk_1` FOREIGN KEY (`route_point_origin`) REFERENCES `route_points` (`id`),
  CONSTRAINT `points_time_cost_ibfk_2` FOREIGN KEY (`route_point_dest`) REFERENCES `route_points` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `points_time_cost` (`id`, `route_point_origin`, `route_point_dest`, `time`, `cost`, `is_active`) VALUES
(1,	1,	3,	1,	20,	1),
(2,	3,	2,	1,	12,	1),
(3,	1,	5,	30,	5,	1),
(4,	1,	8,	10,	1,	1),
(5,	8,	5,	30,	1,	1),
(6,	5,	4,	3,	5,	1),
(7,	4,	6,	4,	50,	1),
(8,	6,	9,	45,	50,	1),
(9,	9,	2,	65,	5,	1),
(10,	6,	7,	40,	50,	1),
(11,	7,	2,	64,	73,	1);

DROP TABLE IF EXISTS `route_points`;
CREATE TABLE `route_points` (
  `id` int NOT NULL AUTO_INCREMENT,
  `point_letter` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `route_points` (`id`, `point_letter`) VALUES
(1,	'A'),
(2,	'B'),
(3,	'C'),
(4,	'D'),
(5,	'E'),
(6,	'F'),
(7,	'G'),
(8,	'H'),
(9,	'I');

DROP TABLE IF EXISTS `user_permissions`;
CREATE TABLE `user_permissions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `permission_id` int NOT NULL,
  `is_enabled` tinyint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `permission_id` (`permission_id`),
  CONSTRAINT `user_permissions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `user_permissions_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `user_permissions` (`id`, `user_id`, `permission_id`, `is_enabled`) VALUES
(1,	1,	1,	1),
(2,	1,	2,	1),
(3,	1,	3,	1),
(4,	1,	4,	1),
(5,	3,	4,	1),
(6,	4,	4,	1);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `username`, `password`, `is_active`) VALUES
(1,	'user_admin',	'827ccb0eea8a706c4c34a16891f84e7b',	1),
(3,	'user_1',	'827ccb0eea8a706c4c34a16891f84e7b',	1),
(4,	'user_2',	'827ccb0eea8a706c4c34a16891f84e7b',	1);

-- 2020-04-24 13:36:34
